import "isomorphic-unfetch";

import Head from "next/head";
import Link from "next/link";
import React from "react";

export default class Index extends React.Component {
  static async getInitialProps() {
    // eslint-disable-next-line no-undef
    const res = await fetch("https://api.github.com/repos/zeit/next.js");
    const json = await res.json();
    return { stars: json.stargazers_count };
  }

  render() {
    return (
      <div>
        <Head>
          <title>Bagaimana.ID</title>
          <meta
            name="google-site-verification"
            content="eJtgOBTTTOE5Lf84P_mmurPdIt-O8kkOSJ_4veNPjTM"
          />
        </Head>
        <div>
          <p>Next.js has {this.props.stars} ⭐️</p>
          <Link prefetch href="/preact">
            <a>How about preact?</a>
          </Link>
        </div>
      </div>
    );
  }
}
